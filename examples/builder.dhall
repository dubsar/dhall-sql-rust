let SQL = https://dhall-sql.deno.dev/package.dhall

let builder = SQL.Builder.default

let server = builder.server

let database = builder.database

let role = builder.role

let grant = builder.grant

let schema = builder.schema

let table = builder.table

let col = builder.col

let pk = builder.pk

let fk = builder.fk

let uniq = builder.uniq

let PWD = "'${env:HOME as Text}'"

in  server
      [ database
          "carlo"
          [ schema "api"
          , table
              "api.clara"
              [ col "uta TEXT PRIMARY KEY", col "img BYTEA", uniq [ "img" ] ]
          , table
              "api.mario"
              [ col "pippo TEXT NOT NULL"
              , col "pluto TEXT NULL"
              , pk [ "pippo" ]
              , fk [ "pippo" ] "api.clara" [ "uta" ]
              ]
          ]
      , role "mario" [ "INHERIT" ]
      , role "carlo" [ "LOGIN PASSWORD ${PWD} INHERIT", "ROLE mario" ]
      , grant [ "USAGE" ] [ "SCHEMA api" ] [ "carlo" ]
      ]
