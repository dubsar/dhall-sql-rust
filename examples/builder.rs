use dhall_sql::{validate, validate_str};
use std::{env, fs, process};

fn main() {
  let usage = || {
    println!("Usage: sql [file|local|gitlab]");
    process::exit(0);
  };
  let args: Vec<String> = env::args().collect();
  if args.len() != 2 {
    usage();
  };
  let environment = args[1].as_str();
  let file = format!("{}/examples/builder.dhall", env!("CARGO_MANIFEST_DIR"));
  match environment {
    "file" => {
      process::Command::new("dhall")
        .arg("--explain")
        .arg("--file")
        .arg(file)
        .status()
        .unwrap();
    }
    "local" => {
      let instant = std::time::Instant::now();
      validate(&file);
      println!("{:?}", instant.elapsed());
    }
    "gitlab" => {
      let instant = std::time::Instant::now();
      let code = fs::read(file).unwrap();
      let code = String::from_utf8(code).unwrap();
      validate_str(&code);
      println!("{:?}", instant.elapsed());
    }
    _ => usage(),
  }
}
