use super::Schema;
use super::Table;
use super::SQL;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
enum Object {
  Schema(Schema),
  Table(Table),
}

#[derive(Serialize, Deserialize)]
pub struct Database {
  name: String,
  objects: Vec<Object>,
  pub sql: SQL,
}
