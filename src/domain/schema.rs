use super::SQL;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct Schema {
  name: String,
  pub sql: SQL,
}
