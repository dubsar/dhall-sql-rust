use super::Database;
use super::SQL;
use serde::{Deserialize, Serialize};
use std::path::Path;

#[derive(Deserialize, Serialize)]
struct Role {
  name: String,
  clauses: Vec<String>,
  sql: SQL,
}

#[derive(Deserialize, Serialize)]
struct Grant {
  privileges: Vec<String>,
  objects: Vec<String>,
  roles: Vec<String>,
  admin: bool,
  sql: SQL,
}

#[derive(Deserialize, Serialize)]
enum Object {
  Database(Database),
  Role(Role),
  Grant(Grant),
}

#[derive(Serialize, Deserialize)]
pub struct Server {
  objects: Vec<Object>,
}

impl Server {
  pub fn len(&self) -> usize {
    self.objects.len()
  }

  #[must_use]
  pub fn is_empty(&self) -> bool {
    self.objects.is_empty()
  }

  pub fn from_file<P>(path: P) -> Self
  where
    P: AsRef<Path>,
  {
    use serde_dhall::from_file;
    if let Err(err) = from_file(&path).parse::<Self>() {
      println!("{err}");
    }
    from_file(path).parse().unwrap()
  }

  #[allow(clippy::should_implement_trait)]
  pub fn from_str(str: &str) -> Self {
    use serde_dhall::from_str;
    if let Err(err) = from_str(str).parse::<Self>() {
      println!("{err}");
    }
    from_str(str).parse().unwrap()
  }
}
