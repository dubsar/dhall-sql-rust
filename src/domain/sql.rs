use super::Database;
use super::Schema;
use super::Table;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub enum Command {
  Create(String),
  Drop(String),
  Wild(String),
}

pub type SQL = Vec<Command>;

pub trait Sql {
  fn sql(&self) -> &SQL;
}

impl Sql for Table {
  fn sql(&self) -> &SQL {
    &self.sql
  }
}

impl Sql for Schema {
  fn sql(&self) -> &SQL {
    &self.sql
  }
}

impl Sql for Database {
  fn sql(&self) -> &SQL {
    &self.sql
  }
}
