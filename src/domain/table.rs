use super::sql::SQL;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
struct Column {
  sql: String,
}

#[derive(Deserialize, Serialize)]
struct ForeignKey {
  columns: Vec<String>,
  reftable: String,
  refcolumns: Vec<String>,
}

#[derive(Deserialize, Serialize)]
enum Constraint {
  Unique(Vec<String>),
  PrimaryKey(Vec<String>),
  ForeignKey(ForeignKey),
}

#[derive(Deserialize, Serialize)]
enum Object {
  Column(Column),
  Constraint(Constraint),
}

#[derive(Deserialize, Serialize)]
pub struct Table {
  name: String,
  objects: Vec<Object>,
  pub sql: SQL,
}
