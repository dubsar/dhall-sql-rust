mod domain;

use std::path::Path;

pub use domain::*;

pub fn validate<P>(path: P)
where
  P: AsRef<Path>,
{
  let _ = Server::from_file(path);
}

pub fn validate_str(str: &str) {
  let _ = Server::from_str(str);
}
